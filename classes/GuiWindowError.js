var GuiWindowError = function () {

  var windowTopLeft = [610, 290];
  var windowWidth = 700;
  var windowHeight = 500;
  var margin = 60;
  var factor = 1;

  var parent = new GuiWindow(
    [],
    windowTopLeft[0],
    windowTopLeft[1],
    windowWidth,
    windowHeight,
    "#a8a8a8",
    "#a8a8a8",
    1,
    "Error"
  );

  this.handleClick = function (windowController, mouse) {
    parent.handleClick(windowController, mouse);
  };

  this.handleMouseMove = function (mouse) {
    parent.handleMouseMove(mouse);
  };

  this.render = function (context) {
    parent.render(context);
  };

  this.handleResize = function(factorNew) {
    factor = factorNew;
    parent.handleResize(factor);
  };

  this.setMessage = function (message) {
    parent.setItems([
      new ShapeText(
        (windowTopLeft[0] + windowWidth / 2) * factor,
        (windowTopLeft[1] + windowHeight / 2) * factor,
        50 * factor,
        message,
        "white",
        "center",
        "middle",
        (windowWidth - 2 * margin) * factor
      )
    ]);
  };

};

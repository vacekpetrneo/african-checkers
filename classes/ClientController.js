var ClientController = function (dimension, roundsWithoutJumpMax) {

  /* constants */
  var selfRef = this;
  var players = ['W', 'B'];
  var boardLogic = new BoardLogic(dimension);
  var boardStateInit = 'bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb0wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww';
  var promotionCoords = {
    'W' : [],
    'B' : []
  };
  for (var i = 0; i < dimension; i++) {
    promotionCoords['W'].push(i);
    promotionCoords['B'].push(dimension*dimension - 1 - i);
  }

  this.initGame = function() {
    return {
      boardState: boardStateInit,
      currentPlayer: players[0],
      roundNum: 1,
      roundNumLastJump: 1,
      lockedTileIndex: undefined,
      gameOver: false,
      evaluation: undefined,
      children: [],
      childrenEvaluated: false
    };
  };

  var transformGameState = function(gameState, boardState) {
    var result = {
      boardState: boardState,
      currentPlayer: gameState.currentPlayer,
      roundNum: gameState.roundNum,
      roundNumLastJump: gameState.roundNumLastJump,
      lockedTileIndex: undefined,
      gameOver: false,
      evaluation: undefined,
      children: undefined,
      childrenEvaluated: false
    };
    var i;
    for (i = 0; i < gameState.children.length; i++) {
      if (gameState.children[i].boardState === boardState) {
        result.children = gameState.children[i].children;
        break;
      }
    }
    if (i === gameState.children.length) {
      result.children = [];
    }
    return result;
  };

  var resetEvaluations = function (gameState) {
    var stack = [gameState];
    var head;
    while (stack.length > 0) {
      head = stack.pop();
      head.evaluation = undefined;
      head.childrenEvaluated = false;
      for(var i = 0; i < head.children.length; i++) {
        stack.push(head.children[i]);
      }
    }
    reducePlayOptionCache(gameState);
  };
  this.playComputer = function(
    timestampStart,
    timestampLastPause,
    roundNumMax,
    handler,
    processStack
  ) {
    var playOptions;
    var playOptionIndex;
    var method;
    var gameTreeNode;
    if (roundNumMax === processStack[0].roundNum) { // dummy AI level => skip gameTree evaluation processStack
      gameTreeNode = processStack.shift();
    }
    else {
      resetEvaluations(processStack[0]);
    }

    while (processStack.length > 0) {
      gameTreeNode = processStack.shift();
      if (
        gameTreeNode.roundNum >= roundNumMax
          ||
        gameTreeNode.gameOver
      ) {
        // reached furthest acceptable bottom node / leaf => evaluate
        gameTreeNode.evaluation = evaluate(gameTreeNode);
        continue;
      }
      else if (gameTreeNode.children.length === 0) {
        // if we are inspecting the root and there is just 1 option then skip
        playOptions = getPlayOptionsInternal(gameTreeNode);
        if (processStack.length === 0 && playOptions.starts.length <= 1) {
          break;
        }

        // reached an intermediate node => shift back onto stack and evaluate after child nodes
        processStack.unshift(gameTreeNode);
        method = playOptions.isMove ? 'move' : 'jump';
        for (var i = 0; i < playOptions.starts.length; i++) {
          gameTreeNode.children.push(selfRef[method](
            gameTreeNode,
            playOptions.starts[i],
            playOptions.ends[i]
          ));
        }
        continue;
      }
      if (!gameTreeNode.childrenEvaluated) {
        if (processStack.length === 0 && gameTreeNode.children.length <= 1) {
          gameTreeNode.children[0].evaluation = 0;
          break;
        }

        processStack.unshift(gameTreeNode);
        for (var i = 0; i < gameTreeNode.children.length; i++) {
          processStack.unshift(gameTreeNode.children[i]);
        }
        gameTreeNode.childrenEvaluated = true;

        var timestampNow = Date.now();
        if (timestampNow - timestampLastPause > 20) {
          setTimeout(
            selfRef.playComputer,
            0,
            timestampStart,
            timestampNow,
            roundNumMax,
            handler,
            processStack
          );
          return;
        }
        continue;
      }

      playOptionIndex = 0;
      if (gameTreeNode.currentPlayer === 'W') {
        for (var i = 1; i < gameTreeNode.children.length; i++) {
          if (gameTreeNode.children[playOptionIndex].evaluation < gameTreeNode.children[i].evaluation) {
            playOptionIndex = i;
          }
        }
      }
      else {
        for (var i = 1; i < gameTreeNode.children.length; i++) {
          if (gameTreeNode.children[playOptionIndex].evaluation > gameTreeNode.children[i].evaluation) {
            playOptionIndex = i;
          }
        }
      }
      gameTreeNode.evaluation = gameTreeNode.children[playOptionIndex].evaluation;
    }

    playOptions = getPlayOptionsInternal(gameTreeNode);
    // randomly pick one of the best evaluated actions
    if (
      gameTreeNode.children.length <= 1
        ||
      roundNumMax === gameTreeNode.roundNum
    ) {
      playOptionIndex = Math.floor(Math.random() * playOptions.starts.length);
    }
    else {
      var plausibleOpts = [];
      for (var i = 0; i < gameTreeNode.children.length; i++) {
        if (gameTreeNode.evaluation === gameTreeNode.children[i].evaluation) {
          plausibleOpts.push(i);
        }
      }
      playOptionIndex = plausibleOpts[Math.floor(Math.random() * plausibleOpts.length)];
    }

    setTimeout(
      handler,
      0,
      timestampStart,
      playOptions.starts[playOptionIndex],
      playOptions.ends[playOptionIndex],
    );
  };

  var playOptionCache = {
    'W' : {},
    'B' : {}
  };
  var gameOverPlayOpts = {
    starts: [],
    ends: [],
    victims: [],
    isMove: true
  };
  var undefLockedTileIndex = dimension * dimension;
  var getPlayOptionsInternal = function(gameState) {
    if (gameState.gameOver) {
      return gameOverPlayOpts;
    }
    var stateCache;
    if (playOptionCache[gameState.currentPlayer][gameState.boardState] === undefined) {
      playOptionCache[gameState.currentPlayer][gameState.boardState] = {};
      stateCache = playOptionCache[gameState.currentPlayer][gameState.boardState];
      stateCache[undefLockedTileIndex] = boardLogic.getPossibleJumpsByPlayer(
        gameState.boardState,
        gameState.currentPlayer
      );
      if (stateCache[undefLockedTileIndex].starts.length <= 0) {
        // no jump available -> find moves
        stateCache[undefLockedTileIndex] = boardLogic.getPossibleMovesByPlayer(
          gameState.boardState,
          gameState.currentPlayer
        );
      }
    }
    stateCache = playOptionCache[gameState.currentPlayer][gameState.boardState];
    if (
      gameState.lockedTileIndex !== undefined
        &&
      stateCache[gameState.lockedTileIndex] === undefined
    ) {
      stateCache[gameState.lockedTileIndex] = {
        starts: [],
        ends: [],
        victims: [],
        isMove: stateCache[undefLockedTileIndex].isMove
      };
      if (stateCache[gameState.lockedTileIndex].isMove) {
        for (var i = 0; i < stateCache[undefLockedTileIndex].starts.length; i++) {
          if (stateCache[undefLockedTileIndex].starts[i] === gameState.lockedTileIndex) {
            stateCache[gameState.lockedTileIndex].starts.push(gameState.lockedTileIndex);
            stateCache[gameState.lockedTileIndex].ends.push(
              stateCache[undefLockedTileIndex].ends[i]
            );
          }
        }
      }
      else {
        for (var i = 0; i < stateCache[undefLockedTileIndex].starts.length; i++) {
          if (stateCache[undefLockedTileIndex].starts[i] === gameState.lockedTileIndex) {
            stateCache[gameState.lockedTileIndex].starts.push(gameState.lockedTileIndex);
            stateCache[gameState.lockedTileIndex].ends.push(
              stateCache[undefLockedTileIndex].ends[i]
            );
            stateCache[gameState.lockedTileIndex].victims.push(
              stateCache[undefLockedTileIndex].victims[i]
            );
          }
        }
      }
    }
    if (gameState.lockedTileIndex === undefined) {
      return stateCache[undefLockedTileIndex];
    }
    return stateCache[gameState.lockedTileIndex];
  };
  this.getPlayOptions = function(gameState) {
    var playOptions = getPlayOptionsInternal(gameState);
    return {
      starts: playOptions.starts.concat(),
      ends: playOptions.ends.concat(),
      victims: playOptions.victims.concat(),
      isMove: playOptions.isMove
    };
  };

  var reducePlayOptionCache = function(gameState) {
    var countsCurrent = {
      'W' : 0,
      'B' : 0
    };
    var boardStateUpperCase = gameState.boardState.toUpperCase();
    for (var i = 0; i < boardStateUpperCase.length; i++) {
      countsCurrent[boardStateUpperCase[i]] += 1;
    }

    var counts = {
      'W' : 0,
      'B' : 0
    };
    var stateOrigUpperCase;
    for (var player in playOptionCache) {
      for (var stateOrig in playOptionCache[player]) {
        counts['W'] = 0;
        counts['B'] = 0;
        stateOrigUpperCase = stateOrig.toUpperCase();
        for (var i = 0; i < stateOrigUpperCase.length; i++) {
          counts[stateOrigUpperCase[i]] += 1;
        }
        if (
          countsCurrent['W'] < counts['W']
            ||
          countsCurrent['B'] < counts['B']
        ) {
          delete playOptionCache[player][stateOrig];
        }
      }
    }
  };

  var endTurn = function(gameState) {
    gameState.roundNum++;
    gameState.currentPlayer = players[(gameState.roundNum - 1) % players.length];
    gameState.lockedTileIndex = undefined;

    var playOptions = getPlayOptionsInternal(gameState);
    if (playOptions.starts.length === 0) {
      gameState.gameOver = true;
    }
    else if ((gameState.roundNum - gameState.roundNumLastJump) > roundsWithoutJumpMax) {
      gameState.gameOver = true;
    };
    return gameState;
  };

  var getStringFromIndex = function(index) {
    return String.fromCharCode(97 + index % dimension)
        + (Math.floor(index / dimension) + 1);
  };

  this.move = function (gameState, indexStart, indexEnd) {
    if (gameState.gameOver) {
      return gameState;
    }
    if (gameState.lockedTileIndex !== undefined) {
      throw new Error(
        "Cannot move. You must do additional jumps from "
        + getStringFromIndex(gameState.lockedTileIndex)
        + "."
      );
    }
    var playOptions = getPlayOptionsInternal(gameState);
    if (!playOptions.isMove) {
      throw new Error("Cannot move. You have jump(s) available.");
    }
    var indexMove = -1;
    for (var i = 0; i < playOptions.starts.length; i++) {
      if (
        playOptions.starts[i] === indexStart
          &&
        playOptions.ends[i] === indexEnd
      ) {
        indexMove = i;
        break;
      }
    }
    if (indexMove === -1) {
      throw new Error(
        "You cannot move from "
        + getStringFromIndex(indexStart)
        + " to "
        + getStringFromIndex(indexEnd)
        + ". Choose a different end or start tile."
      );
    }

    var boardStateArr = gameState.boardState.split('');
    boardStateArr[indexEnd] = boardStateArr[indexStart];
    boardStateArr[indexStart] = '0';

    // reached promotion tile 
    if (
      boardStateArr[indexEnd] !== boardStateArr[indexEnd].toUpperCase()
        &&
      promotionCoords[gameState.currentPlayer].indexOf(indexEnd) !== -1
    ) {
      boardStateArr[indexEnd] = boardStateArr[indexEnd].toUpperCase();
    }
    var result = transformGameState(gameState, boardStateArr.join(''));
    endTurn(result);
    return result;
  };

  this.jump = function(gameState, indexStart, indexEnd) {
    if (gameState.gameOver) {
      return gameState;
    }
    if (
      gameState.lockedTileIndex !== undefined
      &&
      gameState.lockedTileIndex !== indexStart
    ) {
      throw new Error(
        "Cannot jump from "
        + getStringFromIndex(indexStart)
        + ". You must start from "
        + getStringFromIndex(gameState.lockedTileIndex)
        + "."
      );
    }

    var playOptions = getPlayOptionsInternal(gameState);
    if (playOptions.isMove) {
      throw new Error("Cannot jump. No jumps available.");
    }
    var indexJump = -1;
    for (var i = 0; i < playOptions.starts.length; i++) {
      if (
        playOptions.starts[i] === indexStart
          &&
        playOptions.ends[i] === indexEnd
      ) {
        indexJump = i;
        break;
      }
    }
    if (indexJump === -1) {
      throw new Error(
        "You cannot jump from "
        + getStringFromIndex(indexStart)
        + " to "
        + getStringFromIndex(indexEnd)
        + ". Choose a different end or start tile."
      );
    }

    var boardStateArr = gameState.boardState.split('');
    boardStateArr[indexEnd] = boardStateArr[indexStart];
    boardStateArr[indexStart] = '0';
    boardStateArr[playOptions.victims[indexJump]] = '0';


    // reached promotion tile 
    if (
      boardStateArr[indexEnd] !== boardStateArr[indexEnd].toUpperCase()
        &&
      promotionCoords[gameState.currentPlayer].indexOf(indexEnd) !== -1
    ) {
      boardStateArr[indexEnd] = boardStateArr[indexEnd].toUpperCase();
      var result = transformGameState(gameState, boardStateArr.join(''));
      result.roundNumLastJump = result.roundNum;
      endTurn(result);
      return result;
    }
    var result = transformGameState(gameState, boardStateArr.join(''));
    result.roundNumLastJump = result.roundNum;
    result.lockedTileIndex = indexEnd;

    // no more jumps
    playOptions = getPlayOptionsInternal(result);
    if (playOptions.isMove || playOptions.starts.length <= 0) {
      endTurn(result);
    }
    return result;
  };

  var evaluate = function(gameState) {
    if ((gameState.roundNum - gameState.roundNumLastJump) > roundsWithoutJumpMax) {
      return 0;
    };
    return boardLogic.evaluate(gameState.boardState);
  };

};

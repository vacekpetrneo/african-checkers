var GuiWindow = function (itemsInit, x, y, width, height, color, colorBorder, alpha, headerText) {
  var items = itemsInit;
  var clsBtnSize = 60;
  var background = new ShapeRectangle(x, y, width, height, color, colorBorder, alpha, alpha);

  var isMousedOverClose = false;

  var closeButton = new ShapeRectangle (
    x + width - clsBtnSize,
    y,
    clsBtnSize,
    clsBtnSize,
    "#828282",
    color,
    0,
    0
  );
  var closeButtonText = new ShapeText(
    x + width - clsBtnSize / 2,
    y + clsBtnSize / 2,
    50,
    "X",
    "#828282",
    "center",
    "middle",
    clsBtnSize
  );
  var closeButtonMouseOverText = new ShapeText(
    x + width - clsBtnSize / 2,
    y + clsBtnSize / 2,
    50,
    "X",
    "#000000",
    "center",
    "middle",
    clsBtnSize
  );

  var header = new ShapeText(
    x + (width / 2),
    y + 30,
    36,
    headerText,
    "white",
    "center",
    "middle",
    width - clsBtnSize
  );

  this.handleClick = function (windowController, mouse) {
    var point = mouse.getCoordsWindow();
    if (closeButton.isPointInside(point)) {
      windowController.close();
    }
  };

  this.handleMouseMove = function (mouse) {
    var point = mouse.getCoordsWindow();
    isMousedOverClose = closeButton.isPointInside(point);
  };

  this.render = function (context) {
    background.render(context);
    header.render(context);

    if (items !== undefined) {
      for (var i = 0; i < items.length; i++) {
        items[i].render(context);
      }
    }

    if (isMousedOverClose) {
      closeButtonMouseOverText.render(context);
    }
    else {
      closeButtonText.render(context);
    }
  };

  this.handleResize = function (factor) {
    background.handleResize(factor);
    header.handleResize(factor);
    closeButton.handleResize(factor);
    closeButtonText.handleResize(factor);
    closeButtonMouseOverText.handleResize(factor);

    if (items !== undefined) {
      for (var i = 0; i < items.length; i++) {
        items[i].handleResize(factor);
      }
    }
  };

  this.setItems = function (newItems) {
    items = newItems;
  };

  this.setHeaderText = function(newHeaderText) {
    header.setText(newHeaderText);
  };

};

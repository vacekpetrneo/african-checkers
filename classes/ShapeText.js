var ShapeText = function (xInit, yInit, sizeInit, textInit, color, textAlignInit, textBaselineInit, maxWidthInit) {

  var x = xInit;
  var y = yInit;
  var size = sizeInit;
  var maxWidth = maxWidthInit;
  var text = textInit;
  var factor = 1;

  var textAlign = (textAlignInit === undefined ? "center" : textAlignInit);
  var textBaseline = (textBaselineInit === undefined ? "alphabet" : textBaselineInit);

  this.getCoords = function() {
    return [x, y];
  };

  this.render = function (context) {
    context.globalAlpha = 1;
    context.font = size + "px serif";
    context.fillStyle = color;
    context.textAlign = textAlign;
    context.textBaseline = textBaseline;
    context.fillText(text, x, y, maxWidth);
  };

  this.handleResize = function(factorNew) {
    factor = factorNew;
    x = xInit * factor;
    y = yInit * factor;
    size = sizeInit * factor;
    maxWidth = maxWidthInit * factor;
  };

  this.setText = function(textNew) {
    text = textNew;
  };

  this.setCoords = function(coords) {
    xInit = coords[0];
    yInit = coords[1];
    x = coords[0] * factor;
    y = coords[1] * factor;
  };

};

var GuiMouse = function () {

  var coordsWindow = [0,0];

  this.handleMove = function(mousEvnt) {
    if (
      coordsWindow[0] != mousEvnt.clientX
        ||
      coordsWindow[1] != mousEvnt.clientY
    ) {
      coordsWindow = [mousEvnt.clientX, mousEvnt.clientY];
    }
  };

  this.getCoordsWindow = function () {
    return [].concat(coordsWindow);
  };

};

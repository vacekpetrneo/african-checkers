var ShapeCircle = function (xMidInit, yMidInit, radiusInit, colorFill, colorStroke, alpha) {

  var xMid = xMidInit;
  var yMid = yMidInit;
  var radius = radiusInit;
  var lineWidthInit = 5;
  var lineWidth = lineWidthInit;
  var factor = 1;

  this.render = function (context) {
    context.lineWidth = lineWidth;
    context.globalAlpha = alpha;
    context.strokeStyle = colorStroke;
    context.fillStyle = colorFill;

    context.beginPath();
    context.arc(xMid, yMid, radius, 0, 2 * Math.PI);
    context.closePath();
    context.fill();
    context.stroke();
  };

  this.handleResize = function(factorNew) {
    factor = factorNew;
    xMid = xMidInit * factor;
    yMid = yMidInit * factor;
    radius = radiusInit * factor;
    lineWidth = lineWidthInit * factor;
  };

  this.isPointInside = function (point) {
    var dx = xMid - point[0];
    var dy = yMid - point[1];
    return (dx * dx + dy * dy <= radius * radius);
  };

  this.setCoords = function(coords) {
    xMid = coords[0] * factor;
    yMid = coords[1] * factor;
    xMidInit = coords[0];
    yMidInit = coords[1];
  };

};

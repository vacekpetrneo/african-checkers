var GuiWindowMainBoard = function (dimension, gameState, playOptions) {

  var selectedTileIndex;

  var tileWidth = 1080 / (dimension + 1);
  var legend = [];
  var tiles = [];
  var highlights = {
    'green': [],
    'red' : [],
    'gold' : []
  };
  var highlightRed = new ShapeRectangle(
    -1000,
    -1000,
    tileWidth,
    tileWidth,
    'red',
    'black',
    0.7,
    0
  );
  var highlightGreen = new ShapeRectangle(
    -1000,
    -1000,
    tileWidth,
    tileWidth,
    'green',
    'black',
    0.7,
    0
  );
  var highlightGold = new ShapeRectangle(
    -1000,
    -1000,
    tileWidth,
    tileWidth,
    'gold',
    'black',
    0.7,
    0
  );
  var unitTemplates = {
    '0' : [
      new ShapeCircle(-1000, -1000, 0.35 * tileWidth, 'white', 'black', 0),
      new ShapeText(-1000, -1000, 0.35 * tileWidth, '', 'black', 'center', 'middle', 0.7 * tileWidth)
    ],
    'b' : [
      new ShapeCircle(-1000, -1000, 0.35 * tileWidth, 'black', 'white', 1),
      new ShapeText(-1000, -1000, 0.35 * tileWidth, 'b', 'white', 'center', 'middle', 0.7 * tileWidth)
    ],
    'B' : [
      new ShapeCircle(-1000, -1000, 0.35 * tileWidth, 'black', 'white', 1),
      new ShapeText(-1000, -1000, 0.35 * tileWidth, 'B', 'white', 'center', 'middle', 0.7 * tileWidth)
    ],
    'w' : [
      new ShapeCircle(-1000, -1000, 0.35 * tileWidth, 'white', 'black', 1),
      new ShapeText(-1000, -1000, 0.35 * tileWidth, 'w', 'black', 'center', 'middle', 0.7 * tileWidth)
    ],
    'W' : [
      new ShapeCircle(-1000, -1000, 0.35 * tileWidth, 'white', 'black', 1),
      new ShapeText(-1000, -1000, 0.35 * tileWidth, 'W', 'black', 'center', 'middle', 0.7 * tileWidth)
    ]
  };

  for (var i = 0; i < dimension; i++) {
    legend.push(new ShapeText(
      (i+1.5) * tileWidth,
      tileWidth / 2,
      tileWidth / 2,
      String.fromCharCode(97 + i), //97 = 'a' in ASCII
      'white',
      'center',
      'middle',
      tileWidth
    ));
    legend.push(new ShapeText(
      tileWidth / 2,
      (i+1.5) * tileWidth,
      tileWidth / 2,
      i + 1,
      'white',
      'center',
      'middle',
      tileWidth
    ));
    tiles[i] = [];
    for (var j = 0; j < dimension; j++) {
      tiles[i][j] = new ShapeRectangle(
        (j+1) * tileWidth,
        (i+1) * tileWidth,
        tileWidth,
        tileWidth,
        ((i*dimension + j) % 2) ? 'white' : 'grey',
        'black',
        1,
        1
      );
    }
  }

  var getStringFromIndex = function(index) {
    if (index === undefined) {
      return 'undefined';
    }
    return String.fromCharCode(97 + index % dimension)
        + (Math.floor(index / dimension) + 1);
  };

  this.render = function (context) {
    for (var i = 0; i < legend.length; i++) {
      legend[i].render(context);
    }
    for (var i = 0; i < tiles.length; i++) {
      for (var j = 0; j < tiles[i].length; j++) {
        tiles[i][j].render(context);
      }
    }
    for (var i = 0; i < highlights.green.length; i++) {
      highlightGreen.setCoords(highlights.green[i]);
      highlightGreen.render(context);
    }
    for (var i = 0; i < highlights.red.length; i++) {
      highlightRed.setCoords(highlights.red[i]);
      highlightRed.render(context);
    }
    for (var i = 0; i < highlights.gold.length; i++) {
      highlightGold.setCoords(highlights.gold[i]);
      highlightGold.render(context);
    }
    var char;
    for (var i = 0; i < tiles.length; i++) {
      for (var j = 0; j < tiles[i].length; j++) {
        char = gameState.boardState[i + j * dimension];
        unitTemplates[char][0].setCoords([
          (i+1.5) * tileWidth,
          (j+1.5) * tileWidth
        ]);
        unitTemplates[char][0].render(context);
        unitTemplates[char][1].setCoords([
          (i+1.5) * tileWidth,
          (j+1.5) * tileWidth
        ]);
        unitTemplates[char][1].render(context);
      }
    }
    return this;
  };

  this.handleResize = function(factor) {
    for (var i = 0; i < legend.length; i++) {
      legend[i].handleResize(factor);
    }
    for (var i = 0; i < tiles.length; i++) {
      for (var j = 0; j < tiles[i].length; j++) {
        tiles[i][j].handleResize(factor);
      }
    }
    highlightRed.handleResize(factor);
    highlightGreen.handleResize(factor);
    highlightGold.handleResize(factor);
    var keys = ['0', 'w', 'W', 'b', 'B'];
    for (var i = 0; i < keys.length; i++) {
      unitTemplates[keys[i]][0].handleResize(factor);
      unitTemplates[keys[i]][1].handleResize(factor);
    }
    return this;
  };

  var setHighlights = function(indexArray, color) {
    var x;
    var y;
    for (var i = 0; i < indexArray.length; i++) {
      x = indexArray[i] % dimension;
      y = Math.floor(indexArray[i] / dimension);
      highlights[color].push([
        (x + 1) * tileWidth,
        (y + 1) * tileWidth
      ]);
    }
  };

  var handleSelectTile = function(indexTile) {
    if (indexTile !== undefined) {
      if (
        gameState.lockedTileIndex !== undefined
        &&
        gameState.lockedTileIndex !== indexTile
      ) {
        throw new Error(
          "You must finish jumping from "
          + getStringFromIndex(gameState.lockedTileIndex)
          + ". You cannot select another unit."
        );
      }

      if (gameState.boardState[indexTile].toUpperCase() !== gameState.currentPlayer) {
        throw new Error("You cannot move opponent's units.");
      }

      if (playOptions.starts.indexOf(indexTile) === -1) {
        throw new Error(
          "No possible moves / jumps from selected tile. Select another one."
        );
      }
    }

    selectedTileIndex = indexTile;
    highlights['green'].length = 0;
    highlights['red'].length = 0;
    highlights['gold'].length = 0;
    var indexArray = [];
    if (indexTile !== undefined) {
      setHighlights([indexTile], 'gold');
      for (var i = 0; i < playOptions.starts.length; i++) {
        if (playOptions.starts[i] === indexTile) {
          indexArray.push(playOptions.ends[i]);
        }
      }
    }
    else {
      for (var i = 0; i < playOptions.starts.length; i++) {
        if (indexArray.indexOf(playOptions.starts[i]) === -1) {
          indexArray.push(playOptions.starts[i]);
        }
      }
    }
    setHighlights(
      indexArray,
      playOptions.isMove ? 'green' : 'red'
    );
  };

  var getTileIndexByMouseCoords = function (mouseCoords) {
    var tile;
    for (var x = 0; x < tiles.length; x++) {
      for (var y = 0; y < tiles[x].length; y++) {
        tile = tiles[x][y];
        if (tile.isPointInside(mouseCoords)) {
          return x * dimension + y;
        }
      }
    }
    return undefined;
  };

  this.setGameState = function(gameStateNew, playOptionsNew) {
    gameState = gameStateNew;
    playOptions = playOptionsNew;
    handleSelectTile(gameState.lockedTileIndex);
  };
  this.setGameState(gameState, playOptions);

  this.handleClick = function (mouse, timestampStart, windowMain) {
    var tileIndex = getTileIndexByMouseCoords(mouse.getCoordsWindow());
    if (tileIndex !== undefined) {

      if (tileIndex === selectedTileIndex) {
        handleSelectTile(gameState.lockedTileIndex);
        return this;
      }

      if (gameState.boardState[tileIndex] !== '0') {
        handleSelectTile(tileIndex);
        return this;
      }

      if (selectedTileIndex === undefined) {
        throw new Error("You need to select a unit first.");
      }

      windowMain.playHandler(timestampStart, selectedTileIndex, tileIndex);
    }
    return this;
  };

  this.handleMouseMove = function (mouse) {
  };

};

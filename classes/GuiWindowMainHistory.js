var GuiWindowMainHistory = function (dimension) {

  var tmpColor = "#828282";
  var borderColor = "#a8a8a8";

  var padding = 4;
  var titleHeight = 108;
  var buttonHeight = titleHeight / 2;
  var left = 1134; // 1080 + 54
  var rowsTop = titleHeight + buttonHeight + padding * 2;
  var width = 582; //1920 - 1080 for board - 150 for side buttons - 108 spacing
  var rowWidth = width - padding * 2;
  var rowHeight = 40;
  var rowNum = 19;
  var midIndex = Math.floor((rowNum-1) / 2);

  var background = new ShapeRectangle(
    left,
    0,
    width,
    1080,
    tmpColor,
    borderColor,
    1,
    1
  );
  var cursor = new ShapeRectangle(
    left,
    rowsTop + (rowHeight + padding) * midIndex,
    width,
    rowHeight,
    '#000080',
    '#000080',
    1,
    1
  );
  var title = new ShapeText(
    left + width / 2,
    titleHeight / 2,
    titleHeight / 2,
    'History',
    "white",
    "center",
    "middle",
    rowWidth
  );
  var buttonUpLabel = new ShapeText(
    left + rowWidth / 2,
    titleHeight + padding + buttonHeight / 2,
    32,
    '▲',
    "white",
    "center",
    "middle",
    width
  );
  var buttonUp = new GuiButton(
    left,
    titleHeight + padding,
    width,
    buttonHeight,
    tmpColor,
    borderColor,
    1,
    1,
    buttonUpLabel
  );
  var buttonDownLabel = new ShapeText(
    left + rowWidth / 2,
    1080 - padding - buttonHeight / 2,
    32,
    '▼',
    "white",
    "center",
    "middle",
    width
  );
  var buttonDown = new GuiButton(
    left,
    1080 - padding - buttonHeight,
    width,
    buttonHeight,
    tmpColor,
    borderColor,
    1,
    1,
    buttonDownLabel
  );
  var buttonUpEnabled = false;
  var buttonDownEnabled = false;

  var rows = [];
  for (var i = 0; i < rowNum; i++) {
    rows.push(new ShapeText(
      left + padding,
      rowsTop + (rowHeight + padding) * i + padding,
      32,
      '',
      "white",
      "left",
      "top",
      rowWidth
    ));
  }

  var historyQueue = [];
  var currentIndex = -1;
  var offset = 0;

  this.getUndoLength = function () {
    return currentIndex + 1;
  };

  this.getRedoLength = function () {
    return historyQueue.length - currentIndex - 1;
  };

  this.undo = function () {
    if (currentIndex < 0) {
      return;
    }
    currentIndex--;
    incrementOffset(-offset);
    resetRows();
    var result = [];
    for (var i = 0; i <= currentIndex; i++) {
      result[i] = {
        isMove: historyQueue[i].isMove,
        indexStart: historyQueue[i].indexStart,
        indexEnd: historyQueue[i].indexEnd
      };
    }
    return result;
  };

  this.redo = function() {
    if (historyQueue.length === currentIndex+1) {
      return;
    }
    currentIndex++;
    incrementOffset(-offset);
    resetRows();
    return {
      isMove: historyQueue[currentIndex].isMove,
      indexStart: historyQueue[currentIndex].indexStart,
      indexEnd: historyQueue[currentIndex].indexEnd
    };
  };

  this.handlePlay = function(isMove, indexStart, indexEnd) {
    historyQueue.length = currentIndex+1;
    currentIndex++;
    incrementOffset(-offset);
    historyQueue.push({
      isMove: isMove,
      indexStart: indexStart,
      indexEnd: indexEnd
    });
    resetRows();
  };

  this.handleNewGame = function() {
    historyQueue.length = 0;
    currentIndex = -1;
    incrementOffset(-offset);
    resetRows();
  };

  this.handleResize = function(factor) {
    background.handleResize(factor);
    title.handleResize(factor);
    cursor.handleResize(factor);
    for (var i = 0; i < rows.length; i++) {
      rows[i].handleResize(factor);
    }
    buttonUp.handleResize(factor);
    buttonDown.handleResize(factor);
  };

  this.render = function(context) {
    background.render(context);
    title.render(context);
    if (-midIndex <= offset && offset <= midIndex) {
      cursor.render(context);
    }
    for (var i = 0; i < rows.length; i++) {
      rows[i].render(context);
    }
    if (buttonUpEnabled) {
      buttonUp.render(context);
    }
    if (buttonDownEnabled) {
      buttonDown.render(context);
    }
  };

  this.handleMouseMove = function (mouse) {
    buttonUp.handleMouseMove(mouse);
    buttonDown.handleMouseMove(mouse);
  };

  this.isPointInside = function (point) {
    return background.isPointInside(point);
  };

  this.handleClick = function (mouse) {
    var pointM = mouse.getCoordsWindow();
    if (
      buttonUpEnabled
        &&
      buttonUp.isPointInside(pointM)
    ) {
      incrementOffset(-1);
      resetRows();
      return;
    }
    if (
      buttonDownEnabled
        &&
      buttonDown.isPointInside(pointM)
    ) {
      incrementOffset(1);
      resetRows();
      return;
    }
  };

  var incrementOffset = function (increment) {
    offset += increment;
    cursor.setCoords([
      left,
      rowsTop + (rowHeight + padding) * (midIndex - offset)
    ]);
    buttonUpEnabled = currentIndex - midIndex + offset > 0;
    buttonDownEnabled = currentIndex + midIndex + offset < historyQueue.length - 1;
  };

  var getStringFromIndex = function(index) {
    return String.fromCharCode(97 + index % dimension)
        + (Math.floor(index / dimension) + 1);
  };

  var resetRows = function() {
    for (var i = 0; i < rows.length; i++) {
      rows[i].setText('');
    }
    var start = currentIndex - midIndex + offset;
    for (var i = 0; i < rowNum && i + start < historyQueue.length; i++) {
      if (i + start < 0) {
        continue;
      }
      rows[i].setText(
        getStringFromIndex(historyQueue[start + i].indexStart)
        + ' -> ' + getStringFromIndex(historyQueue[start + i].indexEnd)
      );
    }
  };

  this.getSaveData = function() {
    var result = '' + currentIndex;
    for (var i = 0; i < historyQueue.length; i++) {
      result += ',' + getStringFromIndex(historyQueue[i].indexStart)
              + ',' + getStringFromIndex(historyQueue[i].indexEnd);
    }
    return result;
  };

};

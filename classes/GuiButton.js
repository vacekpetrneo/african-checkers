var GuiButton = function (
  xInit,
  yInit,
  widthInit,
  heightInit,
  colorFillInit,
  colorBorderInit,
  alphaFillInit,
  alphaBorderInit,
  label
) {

  var isMousedOver = false;

  var background = new ShapeRectangle(
    xInit,
    yInit,
    widthInit,
    heightInit,
    colorFillInit,
    colorBorderInit,
    alphaFillInit,
    alphaBorderInit
  );
  var backgroundMouseOver = new ShapeRectangle(
    xInit,
    yInit,
    widthInit,
    heightInit,
    colorBorderInit,
    colorBorderInit,
    alphaBorderInit,
    alphaBorderInit
  );

  this.isPointInside = function (point) {
    return background.isPointInside(point);
  };

  this.handleMouseMove = function (mouse) {
    var point = mouse.getCoordsWindow();
    isMousedOver = background.isPointInside(point);
  };

  this.render = function (context) {
    if (isMousedOver) {
      backgroundMouseOver.render(context);
    }
    else {
      background.render(context);
    }
    label.render(context);
  };

  this.handleResize = function (factor) {
    background.handleResize(factor);
    backgroundMouseOver.handleResize(factor);
    label.handleResize(factor);
  };

};

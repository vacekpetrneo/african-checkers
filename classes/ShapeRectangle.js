var ShapeRectangle = function (xInit, yInit, widthInit, heightInit, colorFillInit, colorStroke, alphaFillInit, alphaStroke) {

  var x = xInit;
  var y = yInit;
  var width = widthInit;
  var height = heightInit;
  var lineWidthInit = 5;
  var lineWidth = lineWidthInit;
  var colorFill = colorFillInit;
  var alphaFill = alphaFillInit;
  var factor = 1;

  this.getCoords = function() {
    return [x, y];
  };

  this.setCoords = function(coords) {
    xInit = coords[0];
    yInit = coords[1];
    x = xInit * factor;
    y = yInit * factor;
  };

  this.isPointInside = function (point) {
    return (
      x <= point[0]
        &&
      point[0] <= (x + width)
        &&
      y <= point[1]
        &&
      point[1] <= (y + height)
    );
  };

  this.setColorFill = function (colorFillNew) {
    colorFill = colorFillNew;
  };

  this.setAlphaFill = function (alphaFillNew) {
    alphaFill = alphaFillNew;
  };

  this.render = function (context) {
    context.lineWidth = lineWidth;
    context.globalAlpha = alphaFill;
    context.fillStyle = colorFill;

    context.beginPath();
    context.fillRect(x, y, width, height);

    context.globalAlpha = alphaStroke;
    context.strokeStyle = colorStroke;
    context.strokeRect(x, y, width, height);
  };

  this.handleResize = function(factorNew) {
    factor = factorNew;
    x = xInit * factor;
    y = yInit * factor;
    width = widthInit * factor;
    height = heightInit * factor;
    lineWidth = lineWidthInit * factor;
  };

};

var GuiWindowMenu = function () {

  var tmpColor = "#a8a8a8";
  var borderColor = "#828282";

  var buttonWidth = 600;
  var buttonHeight = 100;
  var buttonMarginTop = 20;

  var windowWidth = buttonWidth + 100;
  var windowHeadHeight = 50;
  var windowHeight = windowHeadHeight + 6 * buttonHeight + 7 * buttonMarginTop;
  var windowTopLeft = [(1920-windowWidth) /2, (1080-windowHeight) / 2];

  var labelsX = windowTopLeft[0] + windowWidth / 2;
  var buttonsX = windowTopLeft[0] + (windowWidth - buttonWidth) / 2;
  var buttonsYstart = windowTopLeft[1] + windowHeadHeight;

  var buttonDisplayNewGameLabel = new ShapeText(
    labelsX,
    buttonsYstart + buttonMarginTop * 1 + buttonHeight * 0 + buttonHeight / 2,
    36,
    "New game",
    "white",
    "center",
    "middle",
    buttonWidth
  );
  var buttonDisplayNewGame = new GuiButton(
    buttonsX,
    buttonsYstart + buttonMarginTop * 1 + buttonHeight * 0,
    buttonWidth,
    buttonHeight,
    tmpColor,
    borderColor,
    1,
    1,
    buttonDisplayNewGameLabel
  );

  var buttonDisplaySettingsLabel = new ShapeText(
    labelsX,
    buttonsYstart + buttonMarginTop * 2 + buttonHeight * 1 + buttonHeight / 2,
    36,
    "Game settings",
    "white",
    "center",
    "middle",
    buttonWidth
  );
  var buttonDisplaySettings = new GuiButton(
    buttonsX,
    buttonsYstart + buttonMarginTop * 2 + buttonHeight * 1,
    buttonWidth,
    buttonHeight,
    tmpColor,
    borderColor,
    1,
    1,
    buttonDisplaySettingsLabel
  );

  var buttonDisplaySaveGameLabel = new ShapeText(
    labelsX,
    buttonsYstart + buttonMarginTop * 3 + buttonHeight * 2 + buttonHeight / 2,
    36,
    "Save Game",
    "white",
    "center",
    "middle",
    buttonWidth
  );
  var buttonDisplaySaveGame = new GuiButton(
    buttonsX,
    buttonsYstart + buttonMarginTop * 3 + buttonHeight * 2,
    buttonWidth,
    buttonHeight,
    tmpColor,
    borderColor,
    1,
    1,
    buttonDisplaySaveGameLabel
  );

  var buttonDisplayLoadGameLabel = new ShapeText(
    labelsX,
    buttonsYstart + buttonMarginTop * 4 + buttonHeight * 3 + buttonHeight / 2,
    36,
    "Load Game",
    "white",
    "center",
    "middle",
    buttonWidth
  );
  var buttonDisplayLoadGame = new GuiButton(
    buttonsX,
    buttonsYstart + buttonMarginTop * 4 + buttonHeight * 3,
    buttonWidth,
    buttonHeight,
    tmpColor,
    borderColor,
    1,
    1,
    buttonDisplayLoadGameLabel
  );

  var buttonDisplayRulesLabel = new ShapeText(
    labelsX,
    buttonsYstart + buttonMarginTop * 5 + buttonHeight * 4 + buttonHeight / 2,
    36,
    "Rules",
    "white",
    "center",
    "middle",
    buttonWidth
  );
  var buttonDisplayRules = new GuiButton(
    buttonsX,
    buttonsYstart + buttonMarginTop * 5 + buttonHeight * 4,
    buttonWidth,
    buttonHeight,
    tmpColor,
    borderColor,
    1,
    1,
    buttonDisplayRulesLabel
  );

  var buttonDisplayCreditsLabel = new ShapeText(
    labelsX,
    buttonsYstart + buttonMarginTop * 6 + buttonHeight * 5 + buttonHeight / 2,
    36,
    "Credits",
    "white",
    "center",
    "middle",
    buttonWidth
  );
  var buttonDisplayCredits = new GuiButton(
    buttonsX,
    buttonsYstart + buttonMarginTop * 6 + buttonHeight * 5,
    buttonWidth,
    buttonHeight,
    tmpColor,
    borderColor,
    1,
    1,
    buttonDisplayCreditsLabel
  );

  var items = [
    buttonDisplayNewGame,
    buttonDisplaySettings,
    buttonDisplaySaveGame,
    buttonDisplayLoadGame,
    buttonDisplayRules,
    buttonDisplayCredits
  ];

  var parent = new GuiWindow(
    items,
    windowTopLeft[0],
    windowTopLeft[1],
    windowWidth,
    windowHeight,
    tmpColor,
    tmpColor,
    1,
    "Main Menu"
  );

  this.handleClick = function (windowController, mouse) {
    var pointM = mouse.getCoordsWindow();
    if (buttonDisplayNewGame.isPointInside(pointM)) {
      windowController.displayWindowSettings(true);
      return;
    }
    if (buttonDisplaySettings.isPointInside(pointM)) {
      windowController.displayWindowSettings(false);
      return;
    }
    if (buttonDisplaySaveGame.isPointInside(pointM)) {
      handleSave(windowController);
      return;
    }
    if (buttonDisplayLoadGame.isPointInside(pointM)) {
      handleLoadGame(windowController);
      return;
    }
    if (buttonDisplayRules.isPointInside(pointM)) {
      window.open('africka-dama.pdf');
      return;
    }
    if (buttonDisplayCredits.isPointInside(pointM)) {
      windowController.displayWindowCredits();
      return;
    }

    parent.handleClick(windowController, mouse);
  };

  this.handleMouseMove = function (mouse) {
    for (var i in items) {
      items[i].handleMouseMove(mouse);
    }
    parent.handleMouseMove(mouse);
  };

  this.render = function (context) {
    parent.render(context);
  };

  this.handleResize = function (factor) {
    parent.handleResize(factor);
  };

  var handleSave = function (windowController) {
    var blob = new Blob(
      [windowController.getSaveData()],
      { type: "text/plain;charset=utf-8" }
    );
    var date = new Date();
    var a = document.createElement('a');
    a.download = 'AfrickaDama_' + date.getFullYear()
      + '-' + date.getMonth().toString().padStart(2, 0)
      + '-' + date.getDate().toString().padStart(2, 0)
      + '_' + date.getHours().toString().padStart(2, 0)
      + '-' + date.getMinutes().toString().padStart(2, 0)
      + '-' + date.getSeconds().toString().padStart(2, 0)
      + '.txt';
    a.rel = 'noopener';
    a.target = '_blank';
    a.href = URL.createObjectURL(blob);
    a.dispatchEvent(new MouseEvent('click'));
    URL.revokeObjectURL(a.href);
  };

  var handleLoadGame = function(windowCtrl) {
    var input = document.createElement('input');
    input.type = 'file';
    input.onchange = function(e) {
      var fileReader = new FileReader();
      fileReader.onload = function(e) {
        windowCtrl.handleGameLoad(fileReader.result);
      };
      fileReader.readAsText(e.target.files[0]);
    };
    input.dispatchEvent(new MouseEvent('click'));
  };

};

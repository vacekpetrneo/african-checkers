var GuiWindowGameSettings = function () {

  var isForNewGame;
  var playerWhiteComputerLevel = -1;
  var playerBlackComputerLevel = -1;

  var tmpColor = "#a8a8a8";
  var borderColor = "#828282";

  var windowWidth = 750;
  var windowHeight = 840;
  var windowTopLeft = [(1920-windowWidth)/2, (1080-windowHeight)/2];
  var factor = 1;

  var windowPadding = 50;

  var columnWidth = 300;
  var colOneTopLeft = [
    windowTopLeft[0] + windowPadding,
    windowTopLeft[1] + windowPadding
  ];
  var colTwoTopLeft = [
    windowTopLeft[0] + 2 * windowPadding + columnWidth,
    windowTopLeft[1] + windowPadding
  ];

  var unitCircleRadius = 42; // 1080 / 9 * 0.35
  var optionCircleSize = 100;
  var optionCircleRadius = optionCircleSize * 0.35;

  var buttonWidth = columnWidth - optionCircleSize;
  var buttonHeight = 100;
  var buttonMarginTop = 20;

  var whiteLabel = new ShapeText(
    colOneTopLeft[0] + columnWidth / 2,
    colOneTopLeft[1] + 18 + buttonMarginTop,
    36,
    "White", // White player
    "white",
    "center",
    "middle",
    columnWidth
  );
  var whiteUnitCircle = new ShapeCircle(
    colOneTopLeft[0] + columnWidth / 2,
    colOneTopLeft[1] + 36 + 2 * buttonMarginTop + unitCircleRadius,
    unitCircleRadius,
    "white",
    "black",
    1
  );

  var blackLabel = new ShapeText(
    colTwoTopLeft[0] + columnWidth / 2,
    colTwoTopLeft[1] + 18 + buttonMarginTop,
    36,
    "Black", // Black player
    "white",
    "center",
    "middle",
    columnWidth
  );
  var blackUnitCircle = new ShapeCircle(
    colTwoTopLeft[0] + columnWidth / 2,
    colTwoTopLeft[1] + 36 + 2 * buttonMarginTop + unitCircleRadius,
    unitCircleRadius,
    "black",
    "white",
    1
  );

  var applyButtonLabel = new ShapeText(
    windowTopLeft[0] + windowWidth / 2,
    windowTopLeft[1] + windowHeight - buttonHeight/2 - buttonMarginTop,
    36,
    "Použít",
    "white",
    "center",
    "middle",
    buttonWidth
  );
  var applyButton = new GuiButton(
    windowTopLeft[0] + (windowWidth - buttonWidth) / 2,
    windowTopLeft[1] + windowHeight - buttonHeight - buttonMarginTop,
    buttonWidth,
    buttonHeight,
    tmpColor,
    borderColor,
    1,
    1,
    applyButtonLabel
  );

  var items = [
    whiteLabel,
    whiteUnitCircle,
    blackLabel,
    blackUnitCircle,
    applyButton
  ];

  var whiteButtons = [];
  var whiteCircles = [];
  var whiteCircleXs = [];
  var blackButtons = [];
  var blackCircles = [];
  var blackCircleXs = [];

  var labels = [
    "Human",
    "AI Trivial",
    "AI Easy",
    "AI Hard"
  ];
  for (var i = 0; i < labels.length; i++) {
    var buttonTopLeftY = colOneTopLeft[1] + 36 + (i + 3) * buttonMarginTop + unitCircleRadius * 2  + i * buttonHeight;
    var whiteCircle = new ShapeCircle(
      colOneTopLeft[0] + buttonWidth + optionCircleSize / 2,
      buttonTopLeftY + optionCircleSize / 2,
      optionCircleRadius,
      tmpColor,
      "black",
      1
    );
    items.push(whiteCircle);
    whiteCircles.push(whiteCircle);
    var whiteCircleX = new ShapeText(
      colOneTopLeft[0] + buttonWidth + optionCircleSize / 2,
      buttonTopLeftY + 0.5 * buttonHeight,
      36,
      "",
      "black",
      "center",
      "middle",
      optionCircleSize
    );
    items.push(whiteCircleX);
    whiteCircleXs.push(whiteCircleX);
    var whiteButtonLabel = new ShapeText(
      colOneTopLeft[0] + buttonWidth / 2,
      buttonTopLeftY + 0.5 * buttonHeight,
      36,
      labels[i],
      "white",
      "center",
      "middle",
      columnWidth - optionCircleSize
    );
    var whiteButton = new GuiButton(
      colOneTopLeft[0],
      buttonTopLeftY,
      buttonWidth,
      buttonHeight,
      tmpColor,
      borderColor,
      1,
      1,
      whiteButtonLabel
    );
    whiteButtons.push(whiteButton);
    items.push(whiteButton);

    var blackCircle = new ShapeCircle(
      colTwoTopLeft[0] + optionCircleSize / 2,
      buttonTopLeftY + optionCircleSize / 2,
      optionCircleRadius,
      tmpColor,
      "black",
      1
    );
    items.push(blackCircle);
    blackCircles.push(blackCircle);
    var blackCircleX = new ShapeText(
      colTwoTopLeft[0] + optionCircleSize / 2,
      buttonTopLeftY + 0.5 * buttonHeight,
      36,
      "",
      "black",
      "center",
      "middle",
      optionCircleSize
    );
    items.push(blackCircleX);
    blackCircleXs.push(blackCircleX);
    var blackButtonLabel = new ShapeText(
      colTwoTopLeft[0] + optionCircleSize + buttonWidth / 2,
      buttonTopLeftY + 0.5 * buttonHeight,
      36,
      labels[i],
      "white",
      "center",
      "middle",
      columnWidth - 50
    );
    var blackButton = new GuiButton(
      colTwoTopLeft[0] + optionCircleSize,
      buttonTopLeftY,
      buttonWidth,
      buttonHeight,
      tmpColor,
      borderColor,
      1,
      1,
      blackButtonLabel
    );
    blackButtons.push(blackButton);
    items.push(blackButton);
  }

  var parent = new GuiWindow(
    items,
    windowTopLeft[0],
    windowTopLeft[1],
    windowWidth,
    windowHeight,
    "#a8a8a8",
    "#a8a8a8",
    1,
    "New Game Settings"
  );


  var adjustCircleContents = function () {
    for (var i = 0; i < whiteCircleXs.length; i++) {
      whiteCircleXs[i].setText("");
    }
    whiteCircleXs[playerWhiteComputerLevel + 1].setText("X");
    for (var i = 0; i < blackCircleXs.length; i++) {
      blackCircleXs[i].setText("");
    }
    blackCircleXs[playerBlackComputerLevel + 1].setText("X");
  };
  adjustCircleContents();

  this.handleClick = function (windowController, mouse) {
    var pointM = mouse.getCoordsWindow();
    for (var i = 0; i < whiteButtons.length; i++) {
      if (
        whiteButtons[i].isPointInside(pointM)
          ||
        whiteCircles[i].isPointInside(pointM)
      ) {
        playerWhiteComputerLevel = i-1;
        adjustCircleContents();
        return;
      }
    }
    for (var i = 0; i < blackButtons.length; i++) {
      if (
        blackButtons[i].isPointInside(pointM)
          ||
        blackCircles[i].isPointInside(pointM)
      ) {
        playerBlackComputerLevel = i-1;
        adjustCircleContents();
        return;
      }
    }
    if (applyButton.isPointInside(pointM)) {
      windowController.handleApplySettings(
        isForNewGame,
        playerWhiteComputerLevel,
        playerBlackComputerLevel
      );
      return;
    }
    parent.handleClick(windowController, mouse);
  };

  this.handleMouseMove = function (mouse) {
    for (var i in whiteButtons) {
      whiteButtons[i].handleMouseMove(mouse);
    }
    for (var i in blackButtons) {
      blackButtons[i].handleMouseMove(mouse);
    }
    applyButton.handleMouseMove(mouse);
    parent.handleMouseMove(mouse);
  };

  this.render = function (context) {
    parent.render(context);
  };

  this.handleResize = function(factorNew) {
    factor = factorNew;
    parent.handleResize(factor);
  };

  this.setIsForNewGame = function(newIsForNewGame) {
    isForNewGame = newIsForNewGame;
    parent.setHeaderText(
      isForNewGame ? "New Game Settings" : "Current Game Settings"
    );
  };

  this.handleGameLoad = function(playerWhiteComputerLevelNew, playerBlackComputerLevelNew) {
    if (
      playerWhiteComputerLevelNew > labels.length - 2
        ||
      playerBlackComputerLevelNew > labels.length - 2
    ) {
      throw new Error('Save file corrupted');
    }
    playerWhiteComputerLevel = playerWhiteComputerLevelNew;
    playerBlackComputerLevel = playerBlackComputerLevelNew;
    adjustCircleContents();
  };

};

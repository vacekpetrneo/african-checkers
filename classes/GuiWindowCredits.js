var GuiWindowCredits = function () {

  var tmpColor = "#a8a8a8";

  var windowTopLeft = [610, 145];
  var windowWidth = 700;
  var windowHeight = 810;
  var margin = 60;

  var texts = [
    "Programming",
    'Petr "Mask" Vacek'
  ];

  var rowWidthMax = 600;
  var rowMarginTop = 20;
  var rowHeight = (windowHeight - margin) / texts.length - rowMarginTop;

  var items = [];
  for (var i = 0; i < texts.length; i++) {
    items.push(new ShapeText(
      windowTopLeft[0] + windowWidth / 2,
      windowTopLeft[1] + margin + (rowHeight + rowMarginTop) * i + rowHeight / 2,
      32,
      texts[i],
      "white",
      "center",
      "middle",
      rowWidthMax
    ));
  }


  var parent = new GuiWindow(
    items,
    windowTopLeft[0],
    windowTopLeft[1],
    windowWidth,
    windowHeight,
    tmpColor,
    tmpColor,
    1,
    "Credits"
  );

  this.handleClick = function (windowController, mouse) {
    parent.handleClick(windowController, mouse);
  };

  this.handleMouseMove = function (mouse) {
    parent.handleMouseMove(mouse);
  };

  this.render = function (context) {
    parent.render(context);
  };

  this.handleResize = function(factor) {
    parent.handleResize(factor);
  };

};

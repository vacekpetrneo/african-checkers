var BoardLogic = function(dimension) {

  var allVectorsComponents = [
    [ 0, 1],
    [ 1, 1],
    [ 1, 0],
    [ 1,-1],
    [ 0,-1],
    [-1,-1],
    [-1, 0],
    [-1, 1]
  ];
  var allVectors = [];
  for (var i = 0; i < allVectorsComponents.length; i++) {
    allVectors[i] = allVectorsComponents[i][0] + allVectorsComponents[i][1] * dimension;
  }

  var unitVectors = {
    'B' : allVectors,
    'W' : allVectors,
    'b' : [
      1,                // [ 1, 0]
      1 + dimension,    // [ 1, 1]
      dimension,        // [ 0, 1]
      -1 + dimension,   // [-1, 1]
      -1                // [-1, 0]
    ],
    'w' : [
      -dimension,       // [ 0,-1]
      1 - dimension,    // [ 1,-1]
      1,                // [ 1, 0]
      -1,               // [-1, 0]
      -1 - dimension    // [-1,-1]
    ]
  };
  var movementMap = []; // tileIndex => vectorIndex => magnitudeIndex => newTileIndex
  for (var x = 0; x < dimension; x++) {
    for (var y = 0; y < dimension; y++) {
      movementMap[x + y * dimension] = [];
      for (var v = 0; v < allVectors.length; v++) {
        movementMap[x + y * dimension][v] = [];
        for (var m = 1; m < dimension; m++) {
          if (
            x + allVectorsComponents[v][0] * m < 0
              ||
            x + allVectorsComponents[v][0] * m >= dimension
              ||
            y + allVectorsComponents[v][1] * m < 0
              ||
            y + allVectorsComponents[v][1] * m >= dimension
          )  {
            break;
          }
          movementMap[x + y * dimension][v][m] = x + y * dimension + m * allVectors[v];
        }
      }
    }
  }

  this.getPossibleMovesByPlayer = function(boardState, player) {
    var result = {
      starts: [],
      ends: [],
      victims: [],
      isMove: true
    };

    var indexNew;
    var step;
    var directions;
    var v;
    var i;
    var playerLower = player.toLowerCase();
    for (var indexStart = 0; indexStart < boardState.length; indexStart++) {
      if (
        player !== boardState[indexStart]
            &&
        playerLower !== boardState[indexStart]
      ) {
        continue;
      }

      directions = unitVectors[boardState[indexStart]];
      for (i in directions) {
        v = allVectors.indexOf(directions[i]);
        step = 1;
        do {
          indexNew = movementMap[indexStart][v][step];
          if (
            indexNew === undefined // coords out of board
              ||
            boardState[indexNew] !== '0' // tile is occupied
          ) {
            break;
          }
          result.starts.push(indexStart);
          result.ends.push(indexNew);
          step++;
        } while (boardState[indexStart] === player) // promoted unit? repeat
      }
    }
    return result;
  };

  this.getPossibleJumpsByPlayer = function(boardState, player) {
    var result = {
      starts: [],
      ends: [],
      victims: [],
      isMove: false
    };

    var directions;
    var indexNew;
    var step;
    var v;
    var victimIndex;
    var i;
    var playerLower = player.toLowerCase();
    for (var indexStart = 0; indexStart < boardState.length; indexStart++) {
      if (
        player !== boardState[indexStart]
            &&
        playerLower !== boardState[indexStart]
      ) {
        continue;
      }

      directions = unitVectors[boardState[indexStart]];
      for (i in directions) {
        v = allVectors.indexOf(directions[i]);
        step = 0;
        do {
          step++;
          indexNew = movementMap[indexStart][v][step];
          if (
            indexNew === undefined // coords out of board
              ||
            boardState[indexNew] !== '0' // tile is occupied
          ) {
            break;
          }
        } while (boardState[indexStart] === player) // promoted unit? repeat

        // invalid coords
        if (indexNew === undefined) {
          continue;
        }

        victimIndex = indexNew;
        if (
          boardState[victimIndex] === '0' // cannot jump over empty tile
            ||
          boardState[victimIndex].toUpperCase() === player // cannot jump over own units
        ) {
          continue;
        }

        do {
          step++;
          indexNew = movementMap[indexStart][v][step];
          if (
            indexNew === undefined // coords out of board
              ||
            boardState[indexNew] !== '0' // tile is occupied
          ) {
            break;
          }
          result.starts.push(indexStart);
          result.ends.push(indexNew);
          result.victims.push(victimIndex);
        } while (boardState[indexStart] === player) // promoted unit? repeat
      }
    }
    return result;
  };

  var boardValues = {
    '0' : 0,
    'w' : 1,
    'W' : 3,
    'b' : -1,
    'B' : -3
  };
  this.evaluate = function(boardState) {
    var result = 0;
    for (var i = 0; i < boardState.length; i++) {
      result += boardValues[boardState[i]];
    }
    return result;
  };

};

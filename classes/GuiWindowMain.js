var GuiWindowMain = function (windowCtrl) {

  var dimension = 9;
  var roundsWithoutJumpMax = 30;
  var clientCtrl = new ClientController(dimension, roundsWithoutJumpMax);
  var gameState = clientCtrl.initGame();
  var playOptions = clientCtrl.getPlayOptions(gameState);
  var boardGui = new GuiWindowMainBoard(dimension, gameState, playOptions);
  var playerComputerLevels = {
    'W': -1,
    'B': -1
  };
  var history = new GuiWindowMainHistory(dimension);

  var tmpColor = "#828282";

  var borderColor = "#a8a8a8";
  var padding = 4;

  var buttonSize = 150;
  var buttonLabels = [
    "Main menu",
    "Fullscreen",
    "Hint",
    "Undo",
    "Play",
    "Redo"
  ];
  var buttonsEnabled = [];
  var buttons = [];

  for (var i = 0; i < buttonLabels.length; i++) {
    var buttonLabel = new ShapeText(
      1920 - padding - buttonSize / 2,
      (padding + buttonSize) / 2 + buttonSize * i,
      24,
      buttonLabels[i],
      "white",
      "center",
      "middle",
      buttonSize
    );
    var button = new GuiButton(
      1920 - padding - buttonSize,
      padding / 2 + buttonSize * i,
      buttonSize,
      buttonSize,
      tmpColor,
      borderColor,
      1,
      1,
      buttonLabel
    );
    buttons.push(button);
    buttonsEnabled[i] = true;
  }
  buttonsEnabled[3] = false;
  buttonsEnabled[4] = false;
  buttonsEnabled[5] = false;

  this.render = function (context) {
    boardGui.render(context);
    history.render(context);

    for (var i = 0; i < buttons.length; i++) {
      if (buttonsEnabled[i]) {
        buttons[i].render(context);
      }
    }
  };

  var handlePause = function() {
    timestampInterruptAi = Date.now();
    if (
      playerComputerLevels[gameState.currentPlayer] !== -1
        &&
      !gameState.gameOver
    ) {
      buttonsEnabled[4] = true;
    }
  };

  var handleDisplayMenu = function(windowController) {
    handlePause();
    windowController.displayWindowMenu();
  };

  var timestampInterruptAi = Date.now();
  this.handleClick = function (windowController, mouse) {
    var pointM = mouse.getCoordsWindow();
    if (buttons[0].isPointInside(pointM)) {
      handleDisplayMenu(windowController);
      return;
    }
    if (buttons[1].isPointInside(pointM)) {
      handleToggleFullscreen();
      return;
    }
    if (buttons[2].isPointInside(pointM)) {
      handleHintShow();
      return;
    }
    if (
      buttonsEnabled[3] === true
        &&
      buttons[3].isPointInside(pointM)
    ) {
      handleUndo();
      return;
    }
    if (
      buttonsEnabled[4] === true
        &&
      buttons[4].isPointInside(pointM)
    ) {
      buttonsEnabled[4] = false;
      setTimeout(play, 0);
      return;
    }
    if (
      buttonsEnabled[5] === true
        &&
      buttons[5].isPointInside(pointM)
    ) {
      handleRedo();
      return;
    }

    if (history.isPointInside(pointM)) {
      history.handleClick(mouse);
      return;
    }

    timestampInterruptAi = Date.now();
    boardGui.handleClick(mouse, timestampInterruptAi, this);
    this.handleMouseMove(mouse);
  };

  var handleUndo = function() {
    var undoPlayOpts = history.undo();
    gameState = clientCtrl.initGame();
    playOptions = clientCtrl.getPlayOptions(gameState);
    for (var i = 0; i < undoPlayOpts.length; i++) {
      selfRef.playHandler(
        timestampInterruptAi,
        undoPlayOpts[i].indexStart,
        undoPlayOpts[i].indexEnd,
        true,
        true
      );
    }
    boardGui.setGameState(gameState, playOptions);
    handlePause();
    if (history.getUndoLength() === 0) {
      buttonsEnabled[3] = false;
    }
    buttonsEnabled[5] = true;
  };

  var handleRedo = function() {
    var redo = history.redo();
    if (redo === undefined) {
      throw new Error('Cannot REDO. No moves / jumps in queue.');
    }
    selfRef.playHandler(
      timestampInterruptAi,
      redo.indexStart,
      redo.indexEnd,
      true,
      true
    );
    handlePause();
    if (history.getRedoLength() === 0) {
      buttonsEnabled[5] = false;
    }
    buttonsEnabled[3] = true;
  };

  var hintShowHandler = function(timestampStart, indexStart, indexEnd) {
    windowCtrl.displayWindowError(
      'Suggesting ' + getStringFromIndex(indexStart)
      + ' -> ' + getStringFromIndex(indexEnd)
    );
  };
  var handleHintShow = function() {
    if (gameState.gameOver) {
      throw new Error('Game over. Start a new game or undo turns.');
    }
    handlePause();
    var timestampStart = Date.now();
    clientCtrl.playComputer(
      timestampStart,
      timestampStart,
      gameState.roundNum + 2,
      hintShowHandler,
      [gameState]
    );
  };

  var handleToggleFullscreen = function () {
    if (document.body.requestFullscreen) {
      if(document.fullscreenElement) {
        document.exitFullscreen();
      }
      else {
        document.body.requestFullscreen();
      }
    }
    /* Legacy webkit */
    else if (document.body.webkitRequestFullscreen) {
      if (document.body.webkitFullscreenEnabled) {
        document.body.webkitExitFullscreen();
      }
      else {
        document.body.webkitRequestFullscreen();
      }
    }
    /* Legacy Firefox */
    else if (document.body.mozRequestFullScreen) {
      if (document.body.mozFullScreenEnabled) {
        document.body.mozCancelFullScreen();
      }
      else {
        document.body.mozRequestFullScreen();
      }
    }
    /* Legacy MS / IE */
    else if (document.body.mozRequestFullScreen) {
      if (document.body.msFullscreenEnabled) {
        document.body.msExitFullscreen();
      }
      else {
        document.body.msRequestFullscreen();
      }
    }
  };

  this.handleMouseMove = function (mouse) {
    boardGui.handleMouseMove(mouse);
    history.handleMouseMove(mouse);

    for (var i = 0; i < buttons.length; i++) {
      buttons[i].handleMouseMove(mouse);
    }
  };

  this.handleResize = function(factor) {
    boardGui.handleResize(factor);
    history.handleResize(factor);

    for (var i = 0; i < buttons.length; i++) {
      buttons[i].handleResize(factor);
    }
  };

  this.handleApplySettings = function(
    isForNewGame,
    playerWhiteComputerLevel,
    playerBlackComputerLevel,
    ommitPlay
  ) {
    if (isForNewGame) {
      gameState = clientCtrl.initGame();
      playOptions = clientCtrl.getPlayOptions(gameState);
      history.handleNewGame();
      boardGui.setGameState(gameState, playOptions);
      buttonsEnabled[3] = false;
      buttonsEnabled[5] = false;
    }
    playerComputerLevels['W'] = playerWhiteComputerLevel;
    playerComputerLevels['B'] = playerBlackComputerLevel;
    if (ommitPlay !== true) {
      play();
    }
  };

  var getStringFromIndex = function(index) {
    return String.fromCharCode(97 + index % dimension)
        + (Math.floor(index / dimension) + 1);
  };

  this.playHandler = function(timestampStart, indexStart, indexEnd, ommitPlay, ommitHistory) {
    if (timestampInterruptAi > timestampStart) {
      return;
    }
    if (playOptions.isMove) {
      gameState = clientCtrl.move(gameState, indexStart, indexEnd);
    }
    else {
      gameState = clientCtrl.jump(gameState, indexStart, indexEnd);
    }
    playOptions = clientCtrl.getPlayOptions(gameState);
    boardGui.setGameState(gameState, playOptions);
    buttonsEnabled[3] = true;
    windowCtrl.invalidateRender();
    if (ommitHistory !== true) {
      history.handlePlay(playOptions.isMove, indexStart, indexEnd);
    }
    if (ommitPlay !== true) {
      buttonsEnabled[4] = false;
      setTimeout(play, 0);
    }
    if (history.getRedoLength() === 0) {
      buttonsEnabled[5] = false;
    }
  };

  var selfRef = this;

  var mapPlayerCharToColor = {
    'W': 'black',
    'B': 'white'
  };
  var play = function() {
    if (gameState.gameOver) {
      var message = 'Game Over';
      if ((gameState.roundNum - gameState.roundNumLastJump) > roundsWithoutJumpMax) {
        message = "It's a tie. No jump for " + roundsWithoutJumpMax + " rounds.";
      }
      else if (playOptions.starts.length === 0) {
        message = "Game over - " + mapPlayerCharToColor[gameState.currentPlayer]
          + " wins in turn " + (gameState.roundNum-1) + ".";
      }
      windowCtrl.handleError(new Error(message));
      return;
    }

    if (playerComputerLevels[gameState.currentPlayer] === -1) {
      return; //human -> skip
    }

    var timestampStart = Date.now();
    clientCtrl.playComputer(
      timestampStart,
      timestampStart,
      gameState.roundNum + playerComputerLevels[gameState.currentPlayer],
      selfRef.playHandler,
      [gameState]
    );
  };

  this.getSaveData = function() {
    return playerComputerLevels['W']
      + ',' + playerComputerLevels['B']
      + ',' + history.getSaveData();
  };

  this.handleGameLoad = function(
    playerWhiteComputerLevelNew,
    playerBlackComputerLevelNew,
    currentIndex,
    history
  ) {
    var backup = this.getSaveData();
    try {
      this.handleApplySettings(
        true,
        playerWhiteComputerLevelNew,
        playerBlackComputerLevelNew,
        true
      );
      for (var i = 0; i < history.length; i++) {
        this.playHandler(
          timestampInterruptAi,
          history[i].indexStart,
          history[i].indexEnd,
          true
        );
      }
      for (var i = history.length-1; i > currentIndex; i--) {
        handleUndo();
      }
    }
    catch (ex) {
      windowCtrl.handleGameLoad(backup);
      throw ex;
    }
  };

};

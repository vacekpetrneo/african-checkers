var GuiWindowController = function (context) {

  var dimension = 9;
  var selfRef = this;
  var mouse = new GuiMouse();

  var curtain = new ShapeRectangle(0, 0, 1920, 1080, "black", "black", 0.5, 0);

  var windowMain = new GuiWindowMain(this);
  var windowError = new GuiWindowError();
  var windowMenu = new GuiWindowMenu();
  var windowCredits = new GuiWindowCredits();
  var windowSettings = new GuiWindowGameSettings();

  var isRenderInvalidated = true;

  var activeWindows = [
    windowMain
  ];

  this.invalidateRender = function() {
    isRenderInvalidated = true;
  };

  this.handleError = function (err) {
    console.log(err);
    selfRef.displayWindowError(err.message);
  };

  this.close = function() {
    activeWindows.pop();
    activeWindows.pop();
    selfRef.invalidateRender();
  };

  this.displayWindowCredits = function() {
    activeWindows.push(curtain);
    activeWindows.push(windowCredits);
    selfRef.invalidateRender();
  };

  this.displayWindowError = function(message) {
    windowError.setMessage(message);
    activeWindows.push(curtain);
    activeWindows.push(windowError);
    selfRef.invalidateRender();
  };

  this.displayWindowMenu = function() {
    activeWindows.push(curtain);
    activeWindows.push(windowMenu);
    selfRef.invalidateRender();
  };

  this.displayWindowSettings = function(isForNewGame) {
    activeWindows.push(curtain);
    windowSettings.setIsForNewGame(isForNewGame);
    activeWindows.push(windowSettings);
    selfRef.invalidateRender();
  };

  this.getSaveData = function() {
    return windowMain.getSaveData();
  };

  var getIndexFromString = function(string) {
    if (
      string.length !== 2
        ||
      string.charCodeAt(0) < 97
        ||
      string.charCodeAt(0) >= 97 + dimension
        ||
      Number.isNaN(Number(string[1]))
        ||
      string[1] < 1
        ||
      string[1] > dimension
    ) {
      throw new Error('Save file corrupted');
    }
    return string.charCodeAt(0) - 97 + (string[1] - 1) * dimension;
  };

  this.handleGameLoad = function(rawData) {
    try {
      var rawDataSplit = rawData.split(',');
      if (
        rawDataSplit.length % 2 !== 1
          ||
        rawDataSplit.length < 3
      ) {
        throw new Error('Save file corrupted');
      }
      for (var i = 0; i < 3; i++) {
        if (
          Number.isNaN(Number(rawDataSplit[i]))
            ||
          !Number.isInteger(Number(rawDataSplit[i]))
            ||
          rawDataSplit[i] < -1
        ) {
          throw new Error('Save file corrupted');
        }
      }
      if (rawDataSplit[i] > rawDataSplit.length - 4) {
        throw new Error('Save file corrupted');
      }
      var history = [];
      for (var i = 3; i < rawDataSplit.length; i += 2) {
        history.push({
          indexStart: getIndexFromString(rawDataSplit[i]),
          indexEnd: getIndexFromString(rawDataSplit[i+1])
        });
      }
      windowSettings.handleGameLoad(
        Number(rawDataSplit[0]),
        Number(rawDataSplit[1])
      );
      windowMain.handleGameLoad(
        Number(rawDataSplit[0]),
        Number(rawDataSplit[1]),
        Number(rawDataSplit[2]),
        history
      );
      this.close();
    }
    catch (ex) {
      this.handleError(ex);
    }
  };

  var handleClick = function(mousEvnt) {
    mouse.handleMove(mousEvnt);
    try {
      activeWindows[activeWindows.length - 1].handleClick(selfRef, mouse);
    }
    catch (err) {
      selfRef.handleError(err);
    }
    selfRef.invalidateRender();
  };

  var handleMouseMove = function(mousEvnt) {
    mouse.handleMove(mousEvnt);

    activeWindows[activeWindows.length - 1].handleMouseMove(mouse);
    selfRef.invalidateRender();
  };

  this.handleApplySettings = function(
    isForNewGame,
    playerWhiteComputerLevel,
    playerBlackComputerLevel
  ) {
    this.close();
    this.close();
    windowMain.handleApplySettings(
      isForNewGame,
      playerWhiteComputerLevel,
      playerBlackComputerLevel
    );
    selfRef.invalidateRender();
  };

  this.handleResize = function(factor) {
    curtain.handleResize(factor);
    windowMain.handleResize(factor);
    windowError.handleResize(factor);
    windowMenu.handleResize(factor);
    windowCredits.handleResize(factor);
    windowSettings.handleResize(factor);
    selfRef.invalidateRender();
    render();
  };

  var render = function () {
    if (!isRenderInvalidated) {
      return;
    }
    context.clearRect(0, 0, context.canvas.width, context.canvas.height);

    for (var i = 0; i < activeWindows.length; i++) {
      activeWindows[i].render(context);
    }
    isRenderInvalidated = false;
  };

  var renderingLoop = function() {
    window.requestAnimationFrame(renderingLoop);
    render();
  };

  renderingLoop();
  context.canvas.addEventListener('click', handleClick, false);
  context.canvas.addEventListener('mousemove', handleMouseMove, false);

};

  "use strict";

  function initGame() {
    var canvas = document.getElementById('game');
    var context = canvas.getContext('2d');
    var controller = new GuiWindowController(context);

    var handleResize = function () {
      var designedResX = 1920;
      var designedResY = 1080;

      var factor = Math.min(
        window.innerWidth / designedResX,
        window.innerHeight / designedResY
      );

      canvas.width = designedResX * factor;
      canvas.height = designedResY * factor;
      controller.handleResize(factor);
    };

    window.addEventListener('resize', handleResize, false);
    handleResize();
  }
